<?php
include_once '../library/Import.php';
Import::controller('ControllerUsuario');
Import::library('Request');

$controllerUsuario = new ControllerUsuario();
$controllerUsuario->acesso(new Request());


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="styles/styles.css">
		<link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Finger+Paint' rel='stylesheet' type='text/css'>
	</head>

	<body>
		<div class="content-central">
			<div class="box-register box-shadow">
			<div class="box-register-header">
				<div class="title-min">Entre agora mesmo!</div>
				<img alt="" width="590px" src="styles/images/line_separator.png">
			</div>
			<div class="form-components">
				<form action="" method="post">
					<div class="form">
						<label for="email">E-mail: </label>
						<input type="text"  name="email" size="45">
					</div>
		
					<div class="form">
						<label for="senha">Senha: </label>
						<input type="password" name="senha">
					</div>			
					<input class="bt bt-middle" type="submit" name="acesso" value="Acessar">
				</form>
				</div>
			</div>	
		</div>
	</body>
</html>