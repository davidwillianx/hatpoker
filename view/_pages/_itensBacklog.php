<?php 
$path = dirname(dirname(dirname(__FILE__)));
include_once $path.'/library/Import.php';
Import::library('Request');
Import::controller('ControllerItem');
$controllerItem = new ControllerItem();
$controllerItem->buildListElements(new Request());
?>
