<?php
include_once '../library/Import.php';
Import::library('Request');
Import::library('Security');
Import::controller('ControllerUsuarioProjeto');
Security::access();

$controllerUsuarioProjeto = new ControllerUsuarioProjeto();
$request = new Request();
$controllerUsuarioProjeto->registerUsuarioProjeto($request);

?>
<head>
	<title>Membros</title>
	
		<link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Finger+Paint' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../styles/styles.css">
		<script type="text/javascript" src="../styles/js/external/jquery-1.9.1.min.js"></script>
		
		<!-- Formularios dos membros -->
		<script type="text/javascript" src="../styles/js/gerenciar-usuario.js"></script>
		
</head>
<body>
	<div class="nav-min box-shadow">
			<div class="content-nav">
				<div class="logo">
					<div class="fristWord">Hat</div>
						<div class="logo-middle"><img alt="LogoHat" src="../styles/images/hatPoker.png" width="70px"></div>
					<div class="secondWord">Poker</div>
				</div>
				<div class="menu-min">
					<ul id="menu">
						<li><a href="../projeto/<?php echo $request->getKey('idProjeto');?>" id="createProject">Gerenciamento</a></li>
						<li><a href="#box" rel="modal" id="createProject">Novo Item</a></li>
						<li><a href="#usuario" id="profile">Perfil</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="content-central">
		
		<div class="content-central">
<!-- 		<p class="pageID">Membros</p> -->
			<div class="box-project">
			 	<p class="sub-title-box">Membros</p>
				<?php Import::view('_listUsuarioProjeto');?>
			</div>
			<div class="box-notice">
				 <p class="sub-title-box">Novo</p>
				 	<div class="form">
					 	usuario
					 	<input name="invite" type="radio" id="novoUsuario" value="#new">
					 	adicionar
					 	<input name="invite" type="radio" id="adicionar" value="#add">
				 	</div>
				 	<div class="form-components">
					 	<div class="inform">
					 		Para adicionar membros ao projeto escolha uma das op��es 
					 		acima.
					 	</div>
				 	</div>
			</div>
		</div>
	
		
		</div>
		
</body>
