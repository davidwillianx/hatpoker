<?php 
include_once '../library/Import.php';
Import::library('Request');
Import::library('Security');
Import::library('Session');
Security::access();


/* TODO ajustar formato da data */

$request = new Request();
Session::set('idProjeto',$request->getKey('idProjeto'));

?>
<html>
<head>
		<link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Finger+Paint' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../styles/styles.css">
		<script type="text/javascript" src="../styles/js/external/jquery-1.9.1.min.js"></script>
		
		<!-- Scroll Controll -->
		<script type="text/javascript" src="../styles/js/external/jquery.mousewheel.js"></script> 
		<script type="text/javascript" src="../styles/js/external/jquery.jscrollpane.mim.js"></script>
		<script type="text/javascript" src="../styles/js/box-control-scrollbar.js"></script>
		  <!-- Style Scroll Controll -->
		    <link rel="stylesheet" type="text/css" href="../styles/external/jquery.jscrollpane.css">
		  	<link rel="stylesheet" type="text/css" href="../styles/external/scrollbar.css">
		  	
		 <!-- DragDrop Controll -->
		 <script type="text/javascript" src="../styles/js/external/jquery-ui.js"></script>
		 <script type="text/javascript" src="../styles/js/dragdrop.js"></script>	
		 
		 <!-- Janela Modal -->
		 <script type="text/javascript" src="../styles/js/janela_modal.js"></script>	
		 
		 
		 <!-- Arquivos de configuração -->
		 <script type="text/javascript" src="../styles/js/util.js"></script>	
		 
		 <!-- Persistencia do Item -->
		
		 
					      
	</head>

<title>Projeto</title> 

<body>
	<div class="nav-min box-shadow">
			<div class="content-nav">
				<div class="logo">
					<div class="fristWord">Hat</div>
						<div class="logo-middle"><img alt="LogoHat" src="../styles/images/hatPoker.png" width="70px"></div>
					<div class="secondWord">Poker</div>
				</div>
				<div class="menu-min">
					<ul id="menu">
						<li><a href="../gerenciamento" id="createProject">Projetos</a></li>
						<li><a href="#box" rel="modal" id="createProject">Novo Item</a></li>
						<li><a href="../membros/<?php echo $request->getKey('idProjeto');?>" id="createProject">Membros</a></li>
						<!--<li><a href="#usuario" id="profile">Perfil</a></li>-->
						<li><a href="../planning/<?php echo $request->getKey('idProjeto'); ?>">Planning</a></li>
					</ul>
				</div>
			</div>
		</div>
	<div class="content-central">
		<div class="manager">
			<div class="manager-backlog">
				<div class="labelTop">
					Backlog  <a class="bt bt-orange" href="#box" rel="modal">Novo Item</a>
				</div>
				<div class="intensProject">
					<?php  include_once '_pages/_itensBacklog.php';?>
				</div>
			</div>

			<div class="sprint">
				<div class="labelTop">Estimado</div>
					<div id="itensRun" class="sprintItens">
						 <?php  include_once '_pages/_itensEstimados.php'?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</body>
</html>
