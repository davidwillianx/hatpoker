<?php
include '../library/Import.php';
Import::controller('ControllerProjeto');

$controllerProjeto = new ControllerProjeto();
$controllerProjeto->registerProjeto(new Request());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<title>Gerenciamento</title>
		<link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Finger+Paint' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="styles/styles.css">
		<script type="text/javascript" src="styles/js/external/jquery-1.9.1.min.js"></script>
	</head>
	
	<body>
		<div class="nav-min box-shadow">
			<div class="content-nav">
				<div class="logo">
					<div class="fristWord">Hat</div>
						<div class="logo-middle"><img alt="LogoHat" src="styles/images/hatPoker.png" width="70px"></div>
					<div class="secondWord">Poker</div>
				</div>
				<div class="menu-min">
					<ul id="menu">
						<li><a href="#projeto" id="createProject">Criar Projeto</a></li>
						<li><a href="#usuario" id="profile">Perfil</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="content-central">
		<p class="pageID">Gerenciamento</p>
			<div class="box-project">
			 <p class="sub-title-box">Projetos</p>
				 <img alt=""   width="480px" src="styles/images/line_separator.png">
				 <?php Import::view('_projeto');?>
			</div>
			<div class="box-notice">
				 <p class="sub-title-box">Novo Projeto</p>
				 <img alt=""   width="480px" src="styles/images/line_separator.png">
				 	<div class="form-components">
				 	<form action="" method="post">
					 	<div class="form">
					 		<label>T�tulo</label>
					 			<input type="text" name="titulo" size="45">
					 	</div>
					 	<div class="form">		
					 		<label>Descri��o</label>
					 			<textarea name="descricao" rows="7" cols="46"></textarea>
					 	</div>
					 	<div class="form-group">
					 		<label>In�cio</label>
						 		<input type="text" name="dataInicio" size="8">

					<input class="bt bt-middle" type="submit" name="registerProjeto" value="Cadastrar Projeto">
				 	</form>
				 </div>
			</div>
		</div>
		
	</body>
	
</html>