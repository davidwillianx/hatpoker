<?php
include_once '../library/Import.php';
	Import::controller('ControllerUsuario');
	Import::library('Request');
	$controllerUsuario = new ControllerUsuario();
	$controllerUsuario->saveUsuario(new Request());
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="styles/styles.css">
		<link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Finger+Paint' rel='stylesheet' type='text/css'>
	</head>

	<body>
		<div class="content-central">
			<div class="box-register box-shadow">
			<div class="box-register-header">
				<div class="title-min">Registre-se</div>
				<img alt="" width="590px" src="styles/images/line_separator.png">
			</div>
			<div class="form-components">
				<form action="" method="post">
					<div class="form">
						<label for="nome">Nome: </label>
						<input type="text" name="nome" size="60">
					</div>
					<div class="form">
						<label for="email">E-mail: </label>
						<input type="text"  name="email" size="60">
					</div>
		
					<div class="form">
						<label for="senha">Senha: </label>
						<input type="password" name="senha">
					</div>			
					
					<div class="form">
						<label for="confirmacao">Repita sua senha: </label>
						<input type="password" name="confirmacao">
					</div>

					<input class="bt bt-home" type="submit" name="registarUsuario" value="Cadastrar Usuario">
					<input class="bt bt-home" type="reset" value="Limpar Formulário">
				</form>
				</div>
			</div>	
		</div>
	</body>
</html>