 <?php 

 class Item
 {
 	 const STATUS_ESTIMADO = 3;
 	 const STATUS_PARA_ESTIMAR = 1; 
 	private $id;
 	private $idProjeto;
 	private $idTipoItem;
 	private $titulo;
 	private $descricao;
 	private $dataCriacao;
	private $prioridade;
	private $idStatusItem;
	
 	public function setId($id)
 	{
 		$this->id = $id;
 	}
 	
 	public function setIdProjeto($idProjeto)
 	{
 		$this->idProjeto = $idProjeto;
 	}
 	
 	public function setTitulo($titulo)
 	{
 		$this->titulo = $titulo;
 	}

 	public function setDescricao($descricao)
 	{
 		$this->descricao = $descricao;
 	}
 	
 	public function setDataCriacao($dataCriacao)
 	{
 		$this->dataCriacao = $dataCriacao;
 	}
 	
 	public function setIdTipoItem($idTipoItem)
 	{
 		$this->idTipoItem = $idTipoItem;
 	}
 	public function setPrioridade($prioridade)
 	{
 		$this->prioridade = $prioridade;
 	}
 	public function getId()
 	{
 		return $this->id;
 	}
 	public function setidStatusItem($idStatusItem)
 	{
 		$this->idStatusItem =$idStatusItem;	
 	}
 	
 	public function getIdProjeto()
 	{
 		return $this->idProjeto;	
 	}
 	
 	public function getTitulo()
 	{
 		return $this->titulo;
 	}

 	public  function getDescricao()
 	{
 		return $this->descricao;
 	}
 	
 	public function getDataCriacao()
 	{
 		return $this->dataCriacao;
 	}
 		
 	public function getIdTipoItem()
 	{
 		return $this->idTipoItem;
 	}
 	public function getPrioridade()
 	{
 		return $this->prioridade;
 	}
 	public function getIdStatusItem()
 	{
 		return $this->idStatusItem;
 	}
 }
  ?>