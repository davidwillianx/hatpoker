<?php

	class Message
	{
		private $id;
		private $text;
		private $time;
		private $idUsuario;
		private $idProjeto;


		public function setId($id)
		{
			$this->id = $id;
		}

		public function setText($text)
		{
			$this->text = $text;
		}

		public function setTime($time)
		{
			$this->time = $time;
		}

		public function setIdUsuario($idUsuario)
		{
			$this->idUsuario = $idUsuario;
		}

		public function setIdProjeto($idProjeto)
		{
			$this->idProjeto = $idProjeto;
		}

		public function getId()
		{
			return $this->id;
		}
		public function getText()
		{
			return $this->text;
		}
		public function getTime()
		{
			return $this->time;
		}
		public function getIdUsuario()
		{
			return $this->idUsuario;
		}
		public function getIdProjeto()
		{
			return $this->idProjeto;
		}
	}	

 ?>