<?php

class Projeto
{
	
	const OWNER = 1;
	const TEAM = 2;
	
	private $id;
	private $titulo;
	private $descricao;
	private $dataInicio;
	//private $dataFim;
	
	
	public function setId($id)
	{
		$this->id = $id;	
	}
	
	public function setTitulo($titulo)
	{
		$this->titulo = $titulo;
	}
	
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
	}
	
	public function setDataInicio($dataInicio)
	{
		$this->dataInicio = $dataInicio;
	}
	
	/*public function setDataFim($dataFim)
	{
		$this->dataFim = $dataFim;
	}*/
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getTitulo()
	{
		return $this->titulo;
	}
	
	public function getDescricao()
	{
		return $this->descricao;
	}
	
	public function getDataInicio()
	{
		return $this->dataInicio;
	}
	
	public function getDataFim()
	{
		return $this->dataFim;
	}
}