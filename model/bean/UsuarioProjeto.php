<?php

class UsuarioProjeto
{
	private $idUsuario;
	private $idProjeto;
	private $idPerfil;

	public function setIdUsuario($idUsuario)
	{
		$this->idUsuario = $idUsuario;
	}
	public function setIdProjeto($idProjeto)
	{
		$this->idProjeto = $idProjeto;
	}
	public function setIdPerfil($idPerfil)
	{
		$this->idPerfil = $idPerfil;
	}

	public function getIdUsuario()
	{
		return $this->idUsuario;
	}

	public function getIdProjeto()
	{
		return $this->idProjeto;
	}

	public function getIdPerfil()
	{
		return $this->idPerfil;
	}
}