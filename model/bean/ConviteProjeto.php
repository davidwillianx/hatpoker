<?php

class ConviteProjeto
{
	
	const HASH_INVITE_NEW_USER_TO_PROJECT = 'inviteNewUserToProject';
	const HASH_ADD_MEMBER_TO_PROJECT = 'addMemberToProject';
	
	const STATUS_ATIVO = 1;
	const STATUS_INVATIVO = 2;
	
	private	$id;
	private $idProjeto;
	private $idPerfil;
	private $keyInvite;
	private $status;	
	private $idUsuario;
	
	
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function setIdProjeto($idProjeto)
	{
		$this->idProjeto = $idProjeto;
	}
	
	public function setIdPerfil($idPerfil)
	{
		$this->idPerfil =$idPerfil;
	}
	
	public function setKeyInvite($keyInvite)
	{
		$this->keyInvite = $keyInvite;
	}
	
	public function setStatus($status)
	{
		$this->status = $status;
	}
	
	public function setIdUsuario($idUsuario)
	{
		$this->idUsuario = $idUsuario;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getIdProjeto()
	{
		return $this->idProjeto;
	}
	
	public function getIdPerfil()
	{
		return $this->idPerfil;
	}
	
	public function getKeyInvite()
	{
		return $this->keyInvite;
	}
	
	public function getStatus()
	{
		return $this->status;
	}
	
	public function getIdUsuario()
	{
		return $this->idUsuario;
	}
}