<?php
// include_once '../library/Import.php';
Import::library('Request');
Import::library('Session');
Import::dao('ProjetoDao');
Import::bean('Projeto');
Import::action('ActionUsuarioProjeto');

class ActionProjeto
{
	public function getDao()
	{
		return new ProjectDao();
	}

	public function getAllProjetosWithUsuario($idUsuario)
	{
		return $this->getDao()->selectAllProjetoWithUsuario($idUsuario);
	}

	public function getQuantityUsuarioProjeto($idProjeto)
	{
		$actionUsuarioProjeto = new ActionUsuarioProjeto();
		return $actionUsuarioProjeto->getCountUsuarioProjeto($idProjeto);
	}

	public function saveProjeto(Request $request)
	{
		try
		{
			$projeto = $request->buildObject(new Projeto());
			$idProjeto =  $this->getDao()->persistLastId($projeto);
			

			$request->set('idProjeto', $idProjeto);
			$request->set('idUsuario', Session::get('idUsuario'));
			$request->set('idPerfil', Projeto::OWNER);

				
			$actionUsuarioProjeto =  new ActionUsuarioProjeto();
			$vinculaUsuarioProjeto = $actionUsuarioProjeto->save($request);

		}catch (NoExecuteException $error){
			throw new NoExecuteException('Problema no processamento das informações');
		}catch (NoExecuteException $error){
			throw new NoExecuteException('Problema no processamento das informações');
		}catch (NoPersistException $error){
			throw new NoPersistException('Falha ao salvar dados');
		}

	}

	public function  getProjeto(Request $request)
	{
		$projeto = $request->buildObject(new Projeto());
		return $this->getDao()->selectProjeto($projeto);
	}


}