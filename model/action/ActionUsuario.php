<?php
// include_once '../library/Import.php';
Import::dao('UsuarioDao');
Import::library('Navigation');
Import::library('Session');
Import::library('Request');
Import::library('exceptions/NoResultException');
Import::action('ActionConviteProjeto');
Import::bean('Usuario');


Import::library('exceptions/NoResultException');
Import::library('exceptions/NoPersistException');

class ActionUsuario 
{
	
	function getDao()
	{
		return new UsuarioDao();
	}
	
	//TODO renomear método
	public function saveUsuario(Request $request)
	{
		
		$usuario = $request->buildObject(new Usuario());
		$idUsuario =  $this->getDao()->persistLastId($usuario);
		if(!$idUsuario)
			throw new NoPersistException('Falha ao registrar usuário');
		
		return $idUsuario;
	}
	
	public function saveUsuarioComConvite(Request $request)
	{
		$usuario = $request->buildObject(new Usuario());
		$idUsuario = $this->getDao()->insertUsuarioAndReturnId($usuario);
			return $idUsuario;
	}
	/**
	 * @TODO relizar encript da SENHA (MD5)
	 * @TODO verificar retorno por objeto
	 * @TODO modificar nomde deste método
	 */
	public function findUser(Request $request)
	{
		$usuario = $request->buildObject(new Usuario());
		return $this->getDao()->selectWithWhere($usuario,'email = ? AND senha = ?');
	}
		
	public function signUser($usuario)
	{

		Session::set('idUsuario', $usuario->getId());
		Navigation::sendTo('gerenciamento');
	}
	
	public function getUsuarioByEmail(Request $request)
	{
		$usuario = $request->buildObject(new Usuario());
		$usuarioResponse =  $this->getDao()->selectWithWhere($usuario,'email = ?');

		if(!$usuarioResponse)
			throw new NoResultException();
		return $usuarioResponse;
	}
	
	public function getUsuarioById(Request $request)
	{
		$usuario = $request->buildObject(new Usuario());
		$usuarioResponse = $this->getDao()->selectWithWhere($usuario,'id = ?');
		
		if(!$usuarioResponse)
			throw new NoResultException('N&atilde;o foi possivel obter o us&aacute;rio');
		return $usuarioResponse;
	}
	
	public function atualizarSenhaUsuario(Request $request)
	{
		$usuario = $request->buildObject(new Usuario());
		return $this->getDao()->updateSenhaUsuario($usuario);
	}
	
	public function getUsuarioNotProjeto($idProjeto)
	{
		$usuarios = $this->getDao()->selectUsuarioNotProjeto($idProjeto);
		
		if(!$usuarios)
			throw new NoResultException('Nao existem membros disponiveis');
		
		return $usuarios;
	}
}