<?php
// include_once '../library/Import.php';
Import::dao('ConviteProjetoDao');
Import::library('Request');

Import::library('exceptions/NotFoundException');
Import::library('exceptions/NoPersistException');
Import::library('exceptions/NoResultException');

Import::bean('ConviteProjeto');
Import::action('ActionUsuario');
Import::action('ActionUsuarioProjeto');
Import::controller('ControllerMail');

class ActionConviteProjeto
{
	public function getDao()
	{
		return new ConviteProjetoDao();
	}

	/*@TODO xproject VERIFICAR USO DA FUNCAO*/
	public function setConviteMebroUsuario(Request $request)
	{
		$actionUsuario = new ActionUsuario();
		$usuario = $actionUsuario->getUsuarioByEmail($request);

		if($usuario)
		{
			$request->set('idUsuario', $usuario['id']);
			$request->set('keyInvite', md5(ConviteProjeto::HASH_ADD_MEMBER_TO_PROJECT.$request->getKey('idUsuario')));
			$request->set('status', ConviteProjeto::STATUS_ATIVO);
				
			$conviteProjeto = $request->buildObject(new ConviteProjeto());
				
			$conviteProjeto = $this->getDao()->insert($conviteProjeto);
				
			if($conviteProjeto)
			{
				$controllerMail = new ControllerMail();
				$controllerMail->sendMailConviteProjeto($request);

				echo json_encode(array('invite'=>1));
			}
		}else
			echo json_encode(array('invite'=>0));
	}

	public function getAllConviteAtivoByIdUsuario(Request $request)
	{
		$request->set('idUsuario', Session::get('idUsuario'));
		$request->set('status', ConviteProjeto::STATUS_ATIVO);
		$conviteProjeto = $request->buildObject(new ConviteProjeto());

		return $this->getDao()->selectConviteByUsuarioId($conviteProjeto);
	}


	public function isExistConviteProjeto(Request $request)
	{
		$convites = $this->getAllConviteAtivoByIdUsuario($request);
		if($convites)
			echo json_encode(array('invite'=> 1));
		else echo json_encode(array('invite'=>0));
	}

	public function updateConviteAcessoUsuario(Request $request)
	{
		$conviteProjeto = $request->buildObject(new ConviteProjeto());

		$convite =  $this->getDao()->selectConviteProjeto($conviteProjeto);

		if($convite)
		{
			$actionUsuarioProjeto = new ActionUsuarioProjeto();
			$isAtivado = $actionUsuarioProjeto->save($convite->getIdUsuario(), $convite->getIdProjeto(), $convite->getIdPerfil());

			if($isAtivado)
			{
				$request->set('id', $convite->getId());
				if($this->updateStatusConvite($request))
					return true;
			}

		}else return false;
	}
	
	public function getConviteByIdProjetoAndIdUsuario(Request $request)
	{
		$conviteProjeto = $request->buildObject(new ConviteProjeto());
		$conviteProjetoUsuario = $this->getDao()->selectConviteByIdUsuarioAndIdProjeto($conviteProjeto);
		
		if(!$conviteProjetoUsuario)
			throw new NoResultException('Falha ao obter usu&aacute;rio');
		return $conviteProjetoUsuario;

	}

	public function updateStatusConvite(Request $request)
	{
		$request->set('status', ConviteProjeto::STATUS_INVATIVO);
		$conviteProjeto = $request->buildObject(new ConviteProjeto());

		return $this->getDao()->updateStatusConvite($conviteProjeto);
	}

	public function getConviteProjetoByKey(Request $request)
	{
		$conviteProjetp = $request->buildObject(new ConviteProjeto());
		return $this->getDao()->selectConviteProjetoByKey($conviteProjeto);

	}

	public function persistConviteProjeto(Request $request)
	{
		$actionUsuario = new ActionUsuario();
		$usuario = $actionUsuario->getUsuarioByEmail($request);
		
		if($usuario)
		{
			$request->set('idUsuario', $usuario->getId());

		}else
		{
			
			/*@TODO nome da funcao da action (saveUsuarioComConvite) esta fora de contexto*/
			$idUsuario = $actionUsuario->saveUsuarioComConvite($request);
			$request->set('idUsuario', $idUsuario);
		}
			$request->set('keyInvite', md5(ConviteProjeto::HASH_ADD_MEMBER_TO_PROJECT.$request->getKey('idUsuario')));
			$request->set('status', ConviteProjeto::STATUS_ATIVO);

			$conviteProjeto = $request->buildObject(new ConviteProjeto());
	
		 if($this->getDao()->insert($conviteProjeto))
		 {
		 	$actionUsuarioProjeto = new ActionUsuarioProjeto();
		 	$usuarioProjeto = $actionUsuarioProjeto->save($request->getKey('idUsuario'), $request->getKey('idProjeto'), $request->getKey('idPerfil'));

		 	if($usuarioProjeto)
		 	{
			 	$libMail = new LibMail();
			 	$libMail->debugSmtpOff();
			 	$libMail->IsHTML();
			 	$libMail->sendTo($request->getKey('email'));
			 	$libMail->setAltInform('Plataforma de Estimativas Xproject');
			 	$libMail->setSubject('Convite');

			 	$message = 'Voc� foi convidado a participar  do projeto no sistema de estimativas Xproject
							para ter acesso <a href="http://pomodorando.com/xproject/view/inicio/invite.php?option='.$conviteProjeto->getKeyInvite().'">clique</a>';

			 	$libMail->setBodyMail($message);

			 	$libMail->execute();
			 	return true;
		 	}
		}
		return false;
	}

	
	public function setConviteProjetoUsuario(Request $request)
	{
		$actionUsuario =  new ActionUsuario();
		$usuario = $actionUsuario->getUsuarioById($request);
		
		if($usuario)
		{
			$request->set('idUsuario', $usuario->getId());
			$request->set('keyInvite', md5(ConviteProjeto::HASH_ADD_MEMBER_TO_PROJECT.$request->getKey('idUsuario')));
		
			$conviteProjeto =  $request->buildObject(new ConviteProjeto());
			$conviteInserido = $this->getDao()->insert($conviteProjeto);
			
			if(!$conviteInserido)
				throw new NoPersistException('Falha na vincula&ccedil;&atilde;o do membro ao projeto');
		}else throw new NotFoundException('Usu&aacute;rio nao encontrado');
		
	}
	
	public function getUsuarioConviteProjetoByKey(Request $request)
	{
		$request->set('keyInvite', $request->getKey('option'));

		$conviteProjeto = $request->buildObject(new ConviteProjeto());
		return $this->getDao()->selectUsuarioConviteProjetoByKey($conviteProjeto);
	}
}