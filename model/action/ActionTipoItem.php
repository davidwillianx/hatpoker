<?php
// include_once '../library/Import.php';
Import::dao('TipoItemDao');
Import::bean('TipoItem');

class ActionTipoItem 
{
	public function getDao()
	{
		return new TipoItemDao();
	}
	
	public function getAll()
	{
		return $this->getDao()->select(new TipoItem());
	}
}