<?php
// include_once '../library/Import.php';
Import::dao('UsuarioProjetoDao');
Import::bean('UsuarioProjeto');
Import::action('ActionUsuario');
Import::library('Request');
Import::library('exceptions/NoExecuteException');


class ActionUsuarioProjeto
{
	public function getDao()
	{
		return new UsuarioProjetoDao();
	}

	public function save(Request $request)
	{
		try
		{
			$usuarioProjeto = $request->buildObject(new UsuarioProjeto());
			$this->getDao()->persist($usuarioProjeto);

		}catch (NoPersistException $error){
			throw new NoPersistException('Problemas ao víncular usuario ao projeto');
		}
	}

	public function saveUsuarioProjeto(Request $request)
	{
		if($request->getKey('adicionarUsuario'))
			$this->saveUsuarioProjetoRegistred($request);
		if($request->getKey('registarUsuario'))
			$this->saveUsuarioProjetoWithoutRegister($request);
			
	}

	private function saveUsuarioProjetoRegistred(Request $request)
	{
		try{
			$actionUsuario  = new ActionUsuario();
			$idUsuario = $actionUsuario->getUsuarioByEmail($request)->getId();
			$request->set('idUsuario',$idUsuario);
		
		
			$this->getDao()->persist($request->buildObject(new UsuarioProjeto()));

		}catch(NoResultException $error){
			throw new NoResultException('Usuario não cadastrado no sistema');
		}catch(NoPersistException $error){
			throw new NoPersistException('Falha ao vincular usuário ao projeto');
		}
	}
	
	private function  saveUsuarioProjetoWithoutRegister(Request $request)
	{
		try
		{
			$actionUsuario =  new ActionUsuario();
			$idUsuario = $actionUsuario->saveUsuario($request);

			$request->set('idUsuario',$idUsuario);
			$this->getDao()->persist($request->buildObject(new UsuarioProjeto()));

		}catch(NoPersistException $error){
			throw new NoPersistException('Falha ao vincular usuário ao projeto');
		}
	}

	public function getCountUsuarioProjeto($idProjeto)
	{
		$usuarioProjeto = new UsuarioProjeto();
		$usuarioProjeto->setIdProjeto($idProjeto);

		return $this->getDao()->selectCountWithWhere($usuarioProjeto,'idProjeto = ?');
	}

	public function getAllUsuarioProjetoByIdProjeto(Request $request)
	{
		$usuarioProjeto = $request->buildObject(new UsuarioProjeto());
		return $this->getDao()->selectAllUsuarioProjetoByIdProjeto($usuarioProjeto);
	}
	
	public function getAllUsuarioOutProjetoById(Request $request)
	{
		$usuarioProjeto = $request->buildObject(new UsuarioProjeto());
		return $this->getDao()->selectAllUsuarioOutProjeto($usuarioProjeto);
		
	}
	
	public function removeUsuarioProjetoByIdUsuarioAndIdUnidade(Request $request)
	{
		$usuarioProjeto = $request->buildObject(new UsuarioProjeto());
		
		if (!$this->getDao()->deleteUsuarioProjetoByIdUsuarioAndIdProjeto($usuarioProjeto))
			 throw new NoExecuteException();
		return $usuarioProjeto;
	}

	public function getAllUsuarioArrayProjetoByIdProjeto(Request $request)
	{
		$usuarioProjeto = $request->buildObject(new UsuarioProjeto());
		 $usuarios = $this->getDao()->selectAllUsuarioArrayProjetoByIdProjeto($usuarioProjeto);
		 
		 if(!$usuarios)
		 	throw new NotFoundExeception('Não existem usuários disponíveis');
		 return $usuarios;
	}
}