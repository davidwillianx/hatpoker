<?php

Import::dao('MessageDao');
Import::action('ActionUsuarioProjeto');
Import::bean('Usuario');
Import::library('exceptions/NoResultException');
Import::library('exceptions/NoPersistException');

class ActionMessage
{
	private $response = null;
	
	public function  ActionMessage(Request $request)
	{	
		try {
			///Mudar para post//
			switch ($request->getKey('action')) {
				case 'getUsers': 
					$this->response = $this->getUsers($request);
					break;
				case 'getMessage':
					$this->response = $this->getMessage($request);
					break;			
				case 'sendMessage':
					$this->response = $this->sendMessage($request);
				default:
					# code...
					break;
			}
	
			//TODO melhorar a padronização
			echo json_encode($this->response);

		} catch (Exception $error) {
			//Do something;
			echo $error->getMessage().' code '.$error->getCode();
		}		
	
	}

	public function getDao()
	{
		return new MessageDao();
	}

	private function getUsers(Request $request)
	{
		$request->set('idProjeto',Session::get('idProjeto'));
		$actionUsuarioProjeto = new ActionUsuarioProjeto();
		$usuarios =  $actionUsuarioProjeto->getAllUsuarioArrayProjetoByIdProjeto($request);
		$counterUser = count($usuarios);
	
		return array('users'=>$usuarios,'total'=>$counterUser);
	}

	private function getMessage(Request $request)
	{
		$request->releaseKey('lastID','id');
		$request->set('idProjeto',Session::get('idProjeto'));
		$messages = $this->getDao()->getAllMessageByLastIdMoreThat($request->buildObject(new Message()));
		return array('chats'=>$messages);
	}

	private function sendMessage(Request $request)
	{
		
			 $request->set('idUsuario', Session::get('idUsuario'));
			 $request->set('idProjeto', Session::get('idProjeto'));
			 $request->set('time',date('Y:m:d H:m:s'));
			 var_dump($request);

			 $idMessage = $this->getDao()->persistLastId($request->buildObject(new Message()));
			 if(!$idMessage)
			 	throw new NoPersistException('Falha o realizar persistencia');

			return array('id',$idMessage);
	}
}



 ?>