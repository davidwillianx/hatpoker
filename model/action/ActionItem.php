<?php
// include_once '../library/Import.php';
Import::dao('ItemDao');
Import::bean('Item');
Import::library('Request');
Import::library('Json');

class ActionItem
{
	public function getDao()
	{
		return new ItemDao();
	}


	public function salvaStory(Request $request)
	{
		$item = $request->buildObject(new Item());
		$item->setIdStatusItem(Item::STATUS_PARA_ESTIMAR);
		$item->setDataCriacao(date('Y-m-d'));

		return $this->getDao()->persist($item);
	}

	public function getAllStoryProjeto(Request $request)
	{
		$request->set('idStatusItem', Item::STATUS_PARA_ESTIMAR);
		$item = $request->buildObject(new Item());
		return $this->getDao()->selectItensWithStatus($item);
	}

	public function updateStatusItemJson(Request $request)
	{
		try
		{
			$request->set('idStatusItem', Item::STATUS_ESTIMADO);
			$item =  $request->buildObject(new Item());
			$updated = 	$this->getDao()->updateStatus($item);
				
			Json::send(array('success'=>1));
				
		}catch (NoExecuteException $error){
			Json::send(array('success'=>0));
		}
	}

	/**
	 * @return UserStory
	 */
	public function getAllItensWithStatus(Request $request)
	{
		$item = $request->buildObject(new Item());
		return $this->getDao()->selectAllWithWhere($item,'idProjeto = ? AND idStatusItem = ?');
	}
}
?>