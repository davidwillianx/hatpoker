<?php

Import::library('BuildSqlQuery');
Import::config();



class AbstractDao
{
	private $pdoConnection;
	private $stmt;
	protected $sqlQuery;
	private $indexBind;
	

	public function AbstractDao()
	{
		try{
			$this->pdoConnection = new PDO(
			 Configuration::getDnsConnection()
			,Configuration::USER
			,Configuration::PASSWD
			);
		}catch(PDOException $error)
		{
			echo 'Connection Failure';	
		}
		
	}

	/*TODO Throw exceptions Reflect (Class/Method)*/
	/*TODO Remove Id attribute into classReflected*/
	//Lancar NoExecuteException
	public function persist($objectInstance)
	{
		try{

			$buildSqlQuery = new BuildSqlQuery($objectInstance);
			$this->sqlQuery = $buildSqlQuery->insertQuery();

			echo $this->sqlQuery;
			
	 		$this->prepare();
	 		
			$reflectionClass =  new ReflectionClass($objectInstance);
			$properties = $reflectionClass->getProperties(ReflectionProperty::IS_PRIVATE);
			

			foreach ($properties as $property) {
				if($property->getName() != 'id')
				{
					$reflectionMethod =  new ReflectionMethod($objectInstance,'get'.ucwords($property->getName()));
					$this->setParam($reflectionMethod->invoke($objectInstance));
				}
			}

			return $this->stmt->execute();

		}catch(Exception $error){
			throw new NoPersistException();
		}
	}

	public function persistLastId($objectInstance)
	{
		$this->persist($objectInstance);
		return $this->pdoConnection->lastInsertId();
	}

	//Lancar a notFoundExceotion
	public function select($objectInstance)
	{
		$buildSqlQuery = new BuildSqlQuery($objectInstance);
		$this->sqlQuery = $buildSqlQuery->selectQuery();
		

		$this->prepare();
		$this->stmt->execute();
		return $this->stmt->fetchAll(PDO::FETCH_CLASS,strtolower(get_class($objectInstance)));

	}


	//@TODO REFACTORY Lancar a notFoundExceotion
	public function selectArray($objectInstance)
	{
		$buildSqlQuery = new BuildSqlQuery($objectInstance);
		$this->sqlQuery = $buildSqlQuery->selectQuery();

		$this->prepare();
		
		return $this->fetchAll();
	}

	public function selectAllArrayWithWhere($objectInstance,$where)
	{
		$this->selectWhere($objectInstance,$where);
		return $this->fetchAll();	
	}

	//Lancar a notFoundExceotion
	public function selectAllWithWhere($objectInstance,$where){
		$this->selectWhere($objectInstance,$where);
		return $this->stmt->fetchAll(PDO::FETCH_CLASS,strtolower(get_class($objectInstance)));
	}

	public function selectWithWhere($objectInstance,$where)
	{
		$this->selectWhere($objectInstance,$where);
		return $this->stmt->fetchObject(strtolower(get_class($objectInstance)));
	}

	private function selectWhere($objectInstance,$where)
	{
		try
		{
			$buildSqlQuery = new BuildSqlQuery($objectInstance);
			$this->sqlQuery = $buildSqlQuery->selectQueryWithWhere($where);
			
			
			$this->prepare();

			if($where)
			{
				$where = explode(' AND ',$where);
				$where = implode($where);
				$properties = explode(' = ?',$where);	
				array_pop($properties);

			
				foreach ($properties as $property)
				{	
					$reflectionMethod =  new ReflectionMethod($objectInstance,'get'.ucwords($property));
					$this->setParam($reflectionMethod->invoke($objectInstance));
				}
				
			}

			$this->stmt->execute();
			

		}catch(Exception $error)
		{
			echo 'Algum error ocorreu depois vamos melhorar o tratamento'.$error->getMessage();
		}
		
	}

	public function selectCountWithWhere($objectInstance,$where)
	{
		$buildSqlQuery = new BuildSqlQuery($objectInstance);
		$this->sqlQuery = $buildSqlQuery->selectCountWithWhere($where);

		$this->prepare();
		$this->execute();

		return $this->getStmt()->fetchAll();
	}

	public function fetchAll()
	{
		try {
				$this->getStmt()->execute();
					$result = $this->getStmt()->fetchAll(PDO::FETCH_ASSOC);
				$this->getStmt()->closeCursor();
				return $result;

		} catch (PDOException $error) {
			throw new NoResultException();
		}
	}


	public function fetchAllObject($objectName)
	{
		try
		{
			$this->getStmt()->execute();
			$result = $this->getStmt()->fetchAll(PDO::FETCH_CLASS,$objectName);
			$this->getStmt()->closeCursor();
			return $result;
				
		}catch (PDOException $error){
			throw new NoResultException();
		}

		return false;
	}


		public function prepare()
	{
		try{
			$this->setStmt($this->pdoConnection->prepare($this->sqlQuery));
			$this->indexBind = 0;	
		}catch(PDOException $error)
		{
			echo $error->getMessage();
		}
		
	}

	public function setParam($value,$attribute = null)
	{
		$this->getStmt()->bindParam(++$this->indexBind,$value,$attribute);
	}

	public function execute()
	{
		try
		{
			return $this->getStmt()->execute();
				
		}catch (PDOStatement $error)
		{
			echo 'Error de execução: '+$error->getMessage()+ 'Cod:'+$error->getCode();
			//throw new NoExecuteException('Error: Falha na execução do serviço');
			//return false;
		}
	}

	public function executeLastId()
	{
		try
		{
			 $this->getStmt()->execute();
			 return $this->pdoConnection->lastInsertId();
				
		}catch (PDOStatement $error)
		{
			throw new NoExecuteException('Error: Falha na execução do serviço');
			//return false;
		}
	}


	public function getStmt()
	{
		return $this->stmt;
	}

	public function setStmt($stmtPrepared)
	{
		$this->stmt = $stmtPrepared;
	}

	/**@TODO nao esta funcionando
	 * @TODO Novas ações de "START" transação
	*/
	public function startTransac()
	{
		return $this->pdo->beginTransaction();
	}

	/**@TODO nao esta funcionando
	 * @TODO  novas ações de "FINALYZE" transação
	*/
	public function commitTransac()
	{
		$this->pdo->commit();
	}

	/**
	 * @TODO nao esta funcionando
	 */
	public function rollBackTransac()
	{
		$this->pdo->rollBack();
	}
}