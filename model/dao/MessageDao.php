<?php 
	Import::dao('AbstractDao');
	Import::bean('Message');

	class MessageDao extends AbstractDao
	{

		public function getAllMessageByLastIdMoreThat(Message $message)
		{
			$this->sqlQuery = 'SELECT m.*, u.nome as author FROM
								 message m JOIN usuario u ON (m.idUsuario = u.id) 
								 	WHERE m.id > ? AND m.idProjeto = ?';
	
			$this->prepare();	
			$this->setParam($message->getId());
			$this->setParam($message->getIdProjeto());	
	
			return $this->fetchAll();
		}


	}

?>