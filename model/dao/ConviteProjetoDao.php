<?php
// include_once '../library/Import.php';
Import::dao('Dao');
Import::bean('ConviteProjeto');

class ConviteProjetoDao extends Dao
{
	/**
	 * @TODO problema ao realizar o insert
	 */
	public function insert(ConviteProjeto $conviteProjeto)
	{
		$this->sql = 'INSERT INTO conviteprojeto(keyInvite,idProjeto,idPerfil,idUsuario)
				VALUES (?,?,?,?)';
		$this->prepare();

		$this->setParam($conviteProjeto->getKeyInvite());
		$this->setParam($conviteProjeto->getIdProjeto());
		$this->setParam($conviteProjeto->getIdPerfil());
		$this->setParam($conviteProjeto->getIdUsuario());
		
		return $this->execute();
	}

	public function selectConviteByUsuarioId(ConviteProjeto $conviteProjeto)
	{
		$this->sql = 'SELECT * FROM conviteprojeto WHERE idUsuario = ? AND status = ? ';
		$this->prepare();

		$this->setParam($conviteProjeto->getIdUsuario());
		$this->setParam($conviteProjeto->getStatus());

		return $this->fetchAll();
	}

	public function selectConviteProjeto(ConviteProjeto $conviteProjeto)
	{
		$this->sql = 'SELECT * FROM conviteprojeto WHERE idProjeto = ? AND idUsuario = ?' ;
		$this->prepare();

		$this->setParam($conviteProjeto->getIdProjeto());
		$this->setParam($conviteProjeto->getIdUsuario());

		return $this->fetchObject('ConviteProjeto');
	}

	public function selectConviteByIdUsuarioAndIdProjeto(ConviteProjeto $conviteProjeto)
	{
		$this->sql = 'SELECT * FROM conviteprojeto WHERE idProjeto = ? AND idUsuario = ?' ;
		$this->prepare();

		$this->setParam($conviteProjeto->getIdProjeto());
		$this->setParam($conviteProjeto->getIdUsuario());

		return $this->fetchObject('ConviteProjeto');

	}

	public function updateStatusConvite(ConviteProjeto $conviteProjeto)
	{
		$this->sql = 'UPDATE conviteprojeto set status = ? WHERE id = ? AND idUsuario =?';
		$this->prepare();

		$this->setParam($conviteProjeto->getStatus());
		$this->setParam($conviteProjeto->getId());
		$this->setParam($conviteProjeto->getIdUsuario());

		return $this->execute();
	}
	
	public function selectConviteProjetoByKey(ConviteProjeto $conviteProjeto)
	{
		$this->sql = 'SELECT * conviteprojeto WHERE keyInvite = ? ';
		$this->prepare();
		
		$this->setParam($conviteProjeto->getKeyInvite());
		
		return $this->fetch();
	}
	
	public function selectUsuarioConviteProjetoByKey(ConviteProjeto $conviteProjeto)
	{
		$this->sql = 'SELECT c.*, u.*, p.titulo FROM conviteprojeto c JOIN usuario u ON (u.id = c.idUsuario) JOIN projeto p ON (p.id = c.idProjeto) WHERE c.keyInvite = ?';
		
		$this->prepare();
		$this->setParam($conviteProjeto->getKeyInvite());
		
		return $this->fetchObject('Usuario');
	}
}
?>