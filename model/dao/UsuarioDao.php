<?php
// include_once '../library/Import.php';
Import::bean('Usuario');
Import::dao('AbstractDao');


class UsuarioDao extends AbstractDao
{
	
	public function insertUsuario(Usuario  $usuario)
	{
		$this->sql = "INSERT INTO usuario (nome,email,senha) VALUES  (?,?,?)";
		$this->prepare();
		$this->setParam($usuario->getNome());
		$this->setParam($usuario->getEmail());
		$this->setParam($usuario->getSenha());
		
		return $this->executeLastId();
	}
	
	public function insertUsuarioAndReturnId(Usuario  $usuario)
	{
		$this->sql = "INSERT INTO usuario (nome,email,senha) VALUES  (?,?,?)";
		$this->prepare();
		$this->setParam($usuario->getNome());
		$this->setParam($usuario->getEmail());
		$this->setParam($usuario->getSenha());
	
		return $this->executeLastId();
	}
	
	
	public function updateSenhaUsuario( Usuario $usuario)
	{
		$this->sql = 'UPDATE usuario set senha = ? WHERE id = ?';
		$this->prepare();
		
		$this->setParam($usuario->getSenha());
		$this->setParam($usuario->getId());
		
		return $this->execute();
	}
	

	//Remover e utilizar do usuarioProjetoDao
	public function selectUsuarioNotProjeto($idProjeto)
	{
		$this->sql = 'SELECT u.* FROM usuario u WHERE u.id NOT IN( SELECT u.id FROM usuarioprojeto up
					 JOIN usuario u ON (u.id = up.idUsuario) 
						WHERE up.idProjeto = ? UNION
							SELECT u.id FROM conviteprojeto cp JOIN usuario u ON (u.id = cp.idUsuario)
								 WHERE cp.idProjeto = ? ) ';
		$this->prepare();
		
		$this->setParam($idProjeto);
		$this->setParam($idProjeto);
		return $this->fetchAllObject('Usuario');
		
	}
}