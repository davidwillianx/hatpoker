<?php
// include_once '../library/Import.php';
Import::dao('Dao');
Import::dao('AbstractDao');
Import::bean('Projeto');

class ProjectDao extends AbstractDao 
{
	

	
	public function selectProjeto(Projeto $projeto)
	{
		$this->sqlQuery = "SELECT * FROM projeto WHERE id = ?";
		
		$this->prepare();
		$this->setParam($projeto->getId());
		
		return $this->fetch();
	}
	
	public function selectAllProjetoWithUsuario($idUsuario)
	{
		$this->sqlQuery = 'SELECT p.* FROM usuarioProjeto up 
							JOIN projeto p ON (up.idProjeto = p.id) 
								JOIN usuario u ON (up.idUsuario = u.id)
									 WHERE  u.id = ? ';
		$this->prepare();
		$this->setParam($idUsuario);
		
		return $this->fetchAllObject('Projeto');
	}
}
