<?php
// include_once '../library/Import.php';
Import::dao('AbstractDao');
Import::bean('UsuarioProjeto');
Import::bean('Usuario');

class UsuarioProjetoDao extends AbstractDao
{

	
	public function selectAllUsuarioProjetoByIdProjeto(UsuarioProjeto $usuarioProjeto)
	{
		$this->sqlQuery = 'SELECT up.*, u.* FROM usuarioProjeto up JOIN usuario u ON (up.idUsuario = u.id) WHERE up.idProjeto = ?';
		$this->prepare();
		
		$this->setParam($usuarioProjeto->getIdProjeto());
		return $this->fetchAllObject('Usuario');
	}

	public function selectAllUsuarioArrayProjetoByIdProjeto(UsuarioProjeto $usuarioProjeto)
	{
		$this->sqlQuery = 'SELECT up.idProjeto idProjeto,u.id,u.nome,u.email FROM usuarioProjeto up JOIN usuario u ON (up.idUsuario = u.id) WHERE up.idProjeto = ?';
		$this->prepare();
		
		$this->setParam($usuarioProjeto->getIdProjeto());
		return $this->fetchAll();
	}
	

	public function selectAllUsuarioOutProjeto(UsuarioProjeto $usuarioProjeto)
	{
		$this->sql = 'SELECT up.*, u.* FROM usuarioprojeto up JOIN usuario u ON (up.idUsuario = u.id) WHERE up.idProjeto != ?';
		$this->prepare();
		
		$this->setParam($usuarioProjeto->getIdProjeto());
		return $this->fetchAllObject('UsuarioProjeto');
	}
	
	public function deleteUsuarioProjetoByIdUsuarioAndIdProjeto(UsuarioProjeto $usuarioProjeto)
	{
		$this->sql = 'DELETE FROM usuarioprojeto WHERE idUsuario = ? AND idProjeto = ?';
		$this->prepare();
		$this->setParam($usuarioProjeto->getIdUsuario());
		$this->setParam($usuarioProjeto->getIdProjeto());
		
		return $this->execute();
	}
}
