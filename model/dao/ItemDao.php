<?php
// include_once 'Dao.php';
Import::dao('AbstractDao');


class ItemDao extends AbstractDao
{
	
	public function selectUserStory(UserStory $userStory)
	{
		$this->sql = "SELECT * FROM item WHERE idProjeto = ?";
		$this->prepare();
		$this->setParam($userStory->getIdProjeto());
		
		return $this->fetchAll();
	}
	
	public function updateStatus(UserStory $userStory)
	{
		$this->sql = 'UPDATE item set idStatusItem = ? WHERE id = ?';
		$this->prepare();
		$this->setParam($userStory->getIdStatusItem());
		$this->setParam($userStory->getId());

		return $this->execute();
	}

}