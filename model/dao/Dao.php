<?php

Import::config();
class Dao {

	private $pdo;
	protected  $sql;
	private $stmt;
	private $indexList;

	public function Dao() {
		try {
			$this->pdo = new PDO(
				Configuration::getDnsConnection()
				, Configuration::USER
				, Configuration::PASSWD);

		} catch (PDOException $exception) {
			echo $exception->getCode();
		}
	}


	/**@TODO nao esta funcionando
	 * @TODO Novas a��es de "START" transa��o
	*/
	public function startTransac()
	{
		return $this->pdo->beginTransaction();
	}

	/**@TODO nao esta funcionando
	 * @TODO  novas a��es de "FINALYZE" transa��o
	*/
	public function commitTransac()
	{
		$this->pdo->commit();
	}

	/**
	 * @TODO nao esta funcionando
	 */
	public function rollBackTransac()
	{
		$this->pdo->rollBack();
	}

	public function prepare()
	{

		try{
			$this->setStmt($this->pdo->prepare($this->sql));
			$this->indexList = 0;
		}catch (PDOException $error)
		{
			echo $error->getMessage();
		}
	}

	public function execute()
	{
		try
		{
			return $this->getStmt()->execute();
				
		}catch (PDOStatement $error)
		{
			throw new NoExecuteException('Error: Falha na execu��o do servi�o');
			return false;
		}
	}

	public function executeLastId()
	{
		try 
		{
			$this->pdo->beginTransaction();
			$this->execute();
			$id =  $this->pdo->lastInsertId();
			$this->commitTransac();
			return $id;
			
		}catch (PDOException $error)
		{
			throw new NoExecuteException();
			$this->pdo->rollBack();
		}
		
	}

	/**
	 * @TODO testar valores retornados
	 */
	public function fetch()
	{
		$this->getStmt()->execute();
		$result = $this->getStmt()->fetch();
		$this->getStmt()->closeCursor();

		return $result;
	}

	public function fetchObject($objectName)
	{
		try {
			$this->getStmt()->execute();
			$result = $this->getStmt()->fetchObject($objectName);
			$this->getStmt()->closeCursor();
			return $result;

		}catch (PDOException $error){
			throw  new NotFoundException();
		}

		return false;
	}

	public function fetchAllObject($objectName)
	{
		try
		{
			$this->getStmt()->execute();
			$result = $this->getStmt()->fetchAll(PDO::FETCH_CLASS,$objectName);
			$this->getStmt()->closeCursor();
			return $result;
				
		}catch (PDOException $error){
			echo $error->getMessage();
			// 			throw new NoConnectionException();
		}

		return false;
	}

	/**
	 * @TODO testar valores retornados
	 */
	public function fetchAll()
	{
		try{

			$this->getStmt()->execute();
			$result = $this->getStmt()->fetchAll();
			$this->getStmt()->closeCursor();
			return $result;

		}catch(PDOException $error)
		{
			throw new NotFoundException();
		}
	}

	public function getStmt()
	{
		return $this->stmt;
	}

	public function setStmt($stmtPrepared)
	{
		$this->stmt = $stmtPrepared;
	}

	public function setSQL($stringSQL)
	{
		$this->sql = $stringSQL;
	}

	public function setParam($dataBind)
	{
		$this->getStmt()->bindParam(++$this->indexList,$dataBind);
	}
}