<?php

include_once '../../configuracao/Configuracao.php';

class Dao
{

	private $mongoClient;
	private $database;

	function Dao()
	{
		try
		{
			$this->mongoClient = new MongoClient();
			$this->database = $this->mongoClient->selectDB('test');
			
		}catch (MongoConnectionException $erro)
		{
			echo $erro->getMessage();
		}
	}

	private function getMongoConnect()
	{
		return $this->mongoClient;
	}

	public function getDatabase()
	{
		return $this->database;
	}

	private function setCollection($collection)
	{
		return $this->getDatabase()->selectCollection($collection);
	}
	
	public function persist($data,$collection)
	{
		$data =  $this->setCollection($collection)->insert($data);
		         $this->getMongoConnect()->close();
		         return $data;
	}

	public function searchOne($parametro,$collection)
	{
		return $this->setCollection($collection)->findOne($parametro);
	}
	
	public function search($collection)
	{
		$data =  $this->setCollection($collection)->find();
		$this->getMongoConnect()->close();
		return $data;
	}
	
	public function update()
	{

	}

	public function delete()
	{

	}
}