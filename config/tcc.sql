SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `tcc` DEFAULT CHARACTER SET latin1 ;
USE `tcc` ;

-- -----------------------------------------------------
-- Table `tcc`.`usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`usuario` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `nome` VARCHAR(255) NOT NULL ,
  `email` VARCHAR(255) NOT NULL ,
  `senha` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 12;


-- -----------------------------------------------------
-- Table `tcc`.`projeto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`projeto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `titulo` VARCHAR(255) NOT NULL ,
  `descricao` VARCHAR(255) NOT NULL ,
  `dataInicio` DATE NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 9;


-- -----------------------------------------------------
-- Table `tcc`.`conviteprojeto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`conviteprojeto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `keyInvite` VARCHAR(222) NOT NULL ,
  `status` INT(1) NOT NULL DEFAULT '1' ,
  `idPerfil` INT(11) NOT NULL ,
  `idUsuario` INT(11) NOT NULL ,
  `idProjeto` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_conviteprojeto_usuario1` (`idUsuario` ASC) ,
  INDEX `fk_conviteprojeto_projeto1` (`idProjeto` ASC) ,
  CONSTRAINT `fk_conviteprojeto_usuario1`
    FOREIGN KEY (`idUsuario` )
    REFERENCES `tcc`.`usuario` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_conviteprojeto_projeto1`
    FOREIGN KEY (`idProjeto` )
    REFERENCES `tcc`.`projeto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 36;


-- -----------------------------------------------------
-- Table `tcc`.`tipoitem`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`tipoitem` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `descricao` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4;


-- -----------------------------------------------------
-- Table `tcc`.`statusitem`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`statusitem` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `descricao` VARCHAR(233) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4;


-- -----------------------------------------------------
-- Table `tcc`.`item`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`item` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `titulo` VARCHAR(255) NOT NULL ,
  `descricao` VARCHAR(255) NOT NULL ,
  `dataCriacao` DATE NOT NULL ,
  `prioridade` INT(1) NOT NULL ,
  `idProjeto` INT(11) NOT NULL ,
  `idTipoItem` INT(11) NOT NULL ,
  `idStatusItem` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_item_projeto` (`idProjeto` ASC) ,
  INDEX `fk_item_tipoitem1` (`idTipoItem` ASC) ,
  INDEX `fk_item_statusitem1` (`idStatusItem` ASC) ,
  CONSTRAINT `fk_item_projeto`
    FOREIGN KEY (`idProjeto` )
    REFERENCES `tcc`.`projeto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_tipoitem1`
    FOREIGN KEY (`idTipoItem` )
    REFERENCES `tcc`.`tipoitem` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_statusitem1`
    FOREIGN KEY (`idStatusItem` )
    REFERENCES `tcc`.`statusitem` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 31;


-- -----------------------------------------------------
-- Table `tcc`.`perfil`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`perfil` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `nivel` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `tcc`.`usuarioprojeto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`usuarioprojeto` (
  `idUsuario` INT(11) NOT NULL ,
  `idProjeto` INT(11) NOT NULL ,
  `idPerfil` INT(11) NOT NULL ,
  PRIMARY KEY (`idUsuario`, `idProjeto`) ,
  INDEX `fk_usuarioprojeto_projeto1` (`idProjeto` ASC) ,
  INDEX `fk_usuarioprojeto_perfil1` (`idPerfil` ASC) ,
  CONSTRAINT `fk_usuarioprojeto_projeto1`
    FOREIGN KEY (`idProjeto` )
    REFERENCES `tcc`.`projeto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarioprojeto_perfil1`
    FOREIGN KEY (`idPerfil` )
    REFERENCES `tcc`.`perfil` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tcc`.`message`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tcc`.`message` (
  `id` INT NOT NULL ,
  `text` VARCHAR(222) NULL ,
  `time` TIMESTAMP NULL ,
  `idUsuario` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_message_usuario1` (`idUsuario` ASC) ,
  CONSTRAINT `fk_message_usuario1`
    FOREIGN KEY (`idUsuario` )
    REFERENCES `tcc`.`usuario` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
