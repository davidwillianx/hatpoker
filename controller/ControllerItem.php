<?php

// include_once '../library/Import.php';
Import::action('ActionItem');
Import::bean('Item');
Import::library('html/Div');
Import::library('html/Link');
Import::library('Request');
Import::library('Validator');
Import::library('Session');

class ControllerItem
{
	public function getAction()
	{
		return new ActionItem();
	}

	public function acesso()
	{

	}

	public function saveStory(Request $request)
	{
		$validator = new Validator();
		$validator->setElementCondition($request->getKey('idProjeto'), 'Identeticador do Projeto', 'required');
		$validator->setElementCondition($request->getKey('descricao'), 'Descri��o do item', 'required');
		$validator->setElementCondition($request->getKey('idTipoItem'), 'Categoria do item', 'required');
		$validator->setElementCondition($request->getKey('prioridade'), 'Prioridade do item', 'required');
		$validator->setElementCondition($request->getKey('titulo'), 'Titulo do item', 'required');

		if($validator->isValid())
		{
			$save = $this->getAction()->salvaStory($request);
			
			if($save)
				echo json_encode(array('persist'=> 1));
			else
				echo json_encode(array('persist'=> 0));
		}else $validator->showErros();
	}

	public function buildListElements(Request $request)
	{
		if($request->isElement('idProjeto'))
		{
			try
			{
				$request->set('idStatusItem', Item::STATUS_PARA_ESTIMAR);
				$userStoryes = $this->getAction()->getAllItensWithStatus($request);

				if($userStoryes)
					$this->buildList($userStoryes);

			}catch (NotFoundException $error){
				echo $error;
					
			}catch (NoConnectionException $error){
				echo $error;
			}

		}
	}

	public function buildListElementsEstimados()
	{
		try
		{
			$request = new Request();
			$request->set('idStatusItem', Item::STATUS_ESTIMADO);
			$itens = $this->getAction()->getAllItensWithStatus($request);
				
			if($itens)
				$this->buildList($itens);
				
		}catch (NotFoundException $error){
			echo $error;	
		}catch (NoConnectionException $error){
			echo $error;
		}
	}

	private function buildList($itens)
	{
		foreach ($itens as $item )
		{
			Div::divClass('element gradientWhite',$item->getId());
			Div::divClass('image');
			Div::close();
			Div::divClassContent('labelItem',$item->getTitulo());
// 			Div::divClassContent('actionItem',Link::buildALinkImage('#',$item->getId(),'add.png'));
			// 		 	Div::divClassImage('actionItem','edit.png');
			// 		 	Div::divClassImage('actionItem','trash.png');
			Div::close();
		}
	}

}