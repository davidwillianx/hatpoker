<?php
// include_once '../library/Import.php';
Import::action('ActionUsuario');
Import::library('Navigation');
Import::library('html/Div');
Import::library('Request');
Import::library('Validator');

class ControllerUsuario
{

	public function getAction()
	{
		return new ActionUsuario();
	}

	public function saveUsuario(Request $request)
	{
		if($request->isElement('registarUsuario'))
		{
			$validator = new Validator();
			$validator->setElementCondition($request->getKey('nome'),'Nome', 'required');
			$validator->setElementCondition($request->getKey('email'),'Email', 'required');
			$validator->setElementCondition($request->getKey('senha'),'Senha', 'required');

			if($validator->isValid())
			{
				if($this->getAction()->saveUsuario($request))
					Navigation::sendTo('acesso.php');
			}else $validator->showErros();
		}
	}


	public function acesso(Request $request)
	{
		if($request->isElement('acesso'))
		{
			try{

				$validator = new Validator();
				$validator->setElementCondition($request->getKey('email'), 'Email', 'required');
				$validator->setElementCondition($request->getKey('senha'), 'Senha', 'required');

				if($validator->isValid())
				{
					$usuario = $this->getAction()->findUser($request);
					$this->getAction()->signUser($usuario);

				}else $validator->showErros();

			}catch (NotFoundException $error)
			{
				echo $error;
			}


		}
	}

	public function security()
	{
		session_start();
		if(!isset($_SESSION['idUsuario']))
		{
			session_destroy();
			Navigation::sendTo('signin');
		}
	}

	public function atualizaAcessoUsuario(Request $request)
	{
		if($request->isElement('updateUsuario'))
		{
			$validator = new Validator();
			$validator->setElementCondition($request->getKey('senha'),'Senha', 'required;');
			$validator->setElementCondition($request->getKey('confirm'), 'Confirmação de Senha', 'required;');

			if($validator->isValid())
			{
				if($request->getKey('senha') == $request->getKey('confirm'))
				{
					if($this->getAction()->atualizarSenhaUsuario($request))
					{
						Navigation::sendTo('index.php');
					}
					else echo 'falha no procedimento';
				}

			}else $validator->showErros();
		}
	}
	
	public function buildTableUsuariosOutProjeto(Request $request)
	{
		try
		{
			$usuariosOutProjeto = $this->getAction()->getUsuarioNotProjeto($request->getKey('idProjeto'));
			Table::openTable();
			Table::openThead();
			Table::openTr();
			Table::thContent('Nome');
			Table::thContent('Email');
			Table::thContent('Perfil');
			Table::thContent('-');
			Table::closeTr();
			Table::closeThead();
			 
			Table::openBody();
			foreach ($usuariosOutProjeto as $usuarioOutProjeto)
			{
				Table::openTr($usuarioOutProjeto->getId());
				Table::tdContent($usuarioOutProjeto->getNome());
				Table::tdContent($usuarioOutProjeto->getEmail());
				Table::tdContent($this->getOptionPerfil());
				Table::tdContent('<a  class="bt bt-green newMembro" href="#" id="addMembroProjeto">add</a>');
				Table::closeTr();
			}
			Table::closeBody();
			 
			Table::closeTable();
			
		}catch (NoResultException $error){
			echo $error->getMessage();
		}
	}	
	
	public function getOptionPerfil()
	{
		$select = '<select id="idPerfil">
					<option value="0">Selecione</option>
					<option value="1">owner</option>
					<option value="2">team</option>
				</select>';
	
		return $select;
	}
	
}
?>