<?php
// include_once '../library/Import.php';
Import::action('ActionProjeto');
Import::library('html/Div');
Import::library('html/ListHtml');
Import::library('Request');
Import::library('Json');
Import::library('Validator');


class ControllerProjeto
{
	public function getAction()
	{
		return new ActionProjeto();
	}

	public function buildListProjetos()
	{

		if(Session::isElement('idUsuario'))
		{
			$projetos = $this->getAction()->getAllProjetosWithUsuario(Session::get('idUsuario'));
				

			if($projetos)
			{
				foreach ($projetos as $projeto)
				{
					echo '<a href=projeto/'.$projeto->getId().'>';
					Div::divClass('project');
					Div::divClassContent('labelProject','<p>'.$projeto->getTitulo().'</p>');
					Div::divClass('statusProject');
					ListHtml::openList();
					ListHtml::liContent('Itens: 12');
					$membros = $this->getAction()->getQuantityUsuarioProjeto($projeto->getId());
					ListHtml::liContent('Membros: '.$membros);
					ListHtml::liContent('dataInicio: '.$projeto->getDataInicio());
					ListHtml::closeList();
					Div::close();
					Div::close();
					echo '</a>';
				}
			}else
				echo 'nao existem projetos';
		}
	}

	public function registerProjetoJson(Request $request)
	{
		if($request->getKey('titulo'))
		{
			try
			{
				$validator = new Validator();
				$validator->setElementCondition($request->getKey('titulo'), 'Titulo', 'request');
				$validator->setElementCondition($request->getKey('descricao'), 'Descri��o', 'required');
				$validator->setElementCondition($request->getKey('dataInicio'), 'Data de Inicio', 'required');
					
				if($validator->isValid())
				{
					$projetoSalvo = $this->getAction()->saveProjeto($request);
					Json::send(array('persist'=>1));

				}else $validator->showErros();

			}catch (NoPersistException $error){
				Json::send(array ('persist'=>0));
			}catch (NoExecuteException $error){
				Json::send(array ('persist'=>0));
			}
		}
	}
	
	public function registerProjeto(Request $request)
	{
	 	if($request->isElement('registerProjeto'))
	 	{

	 			$validator = new Validator();
	 			$validator->setElementCondition($request->getKey('titulo'), 'Titulo', 'request');
	 			$validator->setElementCondition($request->getKey('descricao'), 'Descri��o', 'required');
	 			$validator->setElementCondition($request->getKey('dataInicio'), 'Data de Inicio', 'required');
	 						
	 			if($validator->isValid())
	 			{
	 				try{
	 					
	 					$this->getAction()->saveProjeto($request);
	 					echo 'Registro realizado com sucesso';
	 					
	 				}catch (NoPersistException $error)
	 				{
	 				 //TODO realizar ERROS de persistencia
	 				}catch (NoExecuteException $error)
	 				{
	 					
	 				}
	 			}else $validator->showErros();
	 		}
	 		
	 	}	
	}
