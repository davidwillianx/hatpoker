<?php
// include_once '../library/Import.php';
Import::action('ActionUsuarioProjeto');
Import::controller('ControllerUsuario');
Import::library('Request');
Import::library('html/Table');
Import::library('html/Link');

class ControllerUsuarioProjeto
{
	public function getAction()
	{
		return new ActionUsuarioProjeto();
	}
	
	public function listAllUsuarioProjeto($request)
	{
		$usuarioProjetos = $this->getAction()->getAllUsuarioProjetoByIdProjeto($request);

		if($usuarioProjetos)
		{
			Table::openTable();
				Table::openThead();
					Table::openTr();
						Table::thContent('Nome');
						Table::thContent('Email');
						Table::thContent('Perfil');
						Table::thContent('Ações');
					Table::closeTr();
				Table::closeThead();
				
				Table::openBody();
				foreach ($usuarioProjetos as $usuarioProjeto)
				{
					Table::openTr();
						Table::tdContent($usuarioProjeto->getNome());
						Table::tdContent($usuarioProjeto->getEmail());
						Table::tdContent($usuarioProjeto->perfil($usuarioProjeto->idPerfil));
						Table::tdContent(Link::buildALinkImage('#remove', '#rmv-'.$usuarioProjeto->getId(), 'remove.png'));
								//TODO realizar operação de edição
								//Link::buildALinkImage('#edit', '#edt-'.$usuarioProjeto->getId(), 'edit.png')
					Table::closeTr();
				}
				Table::closeBody();	
				
			Table::closeTable();
		}
	}



	public function registerUsuarioProjeto(Request $request)
	{
		$validator = new Validator();

		if($request->isElement('adicionarUsuario'))
		{
			$validator = new Validator();
			$validator->setElementCondition($request->getKey('email'), 'Email', 'required;');
		}
		if($request->isElement('registarUsuario'))
		{
			$validator = new Validator();
			$validator->setElementCondition($request->getKey('nome'), 'Nome', 'required;');
			$validator->setElementCondition($request->getKey('email'), 'Email', 'required;');
			$validator->setElementCondition($request->getKey('senha'), 'Senha', 'required;');
			$validator->setElementCondition($request->getKey('confirmacao'), 'Confirmacao', 'required;');
			
		}

		if($validator->isValid())
		{
			try{
					//TODO notificar usuário por email sobre o sucesso da operação
					$this->getAction()->saveUsuarioProjeto($request);
					echo 'Cadastro realizado com sucesso!';

				}catch(NoResultException $error){
					echo $error->getMessage();

				}catch(NoPersistException $error){
					echo $error->getMessage();
				}
		}else $validator->showErros();
	}

	
	public function deleteUsuarioProjeto(Request $request)
	{
		if($request->isElement('idUsuario') && $request->getKey('idProjeto'))
		{
			try{
				$this->getAction()->removeUsuarioProjetoByIdUsuarioAndIdUnidade($request);
				echo json_encode(array('remove'=>1));
			}catch (NoResultException $error)
			{
				echo json_encode(array('remove'=> 0));
			}
			
		}
	}
	
}