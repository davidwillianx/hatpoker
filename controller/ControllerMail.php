<?php
// include_once '../library/Import.php';
Import::library('mail/LibMail');
Import::library('exceptions/NotMailException');
Import::controller('ControllerConviteProjeto');
Import::controller('ControllerUsuario');

class ControllerMail
{

	public function sendMailConviteProjeto(ConviteProjeto $conviteProjeto)
	{
		$controllerUsuario = new ControllerUsuario();
		$request = new Request();
		$request->set('id', $conviteProjeto->getIdUsuario());
		$controllerUsuario->getAction()->getUsuarioById($request);
			$libMail = new LibMail();
			$libMail->IsHTML();
			$libMail->sendTo($usuario['email']); 
			$libMail->setAltInform('Plataforma de Estimativas Xproject');
			$libMail->setSubject('Convite');
			
			$message = 'Voc� foi convidado a participar  do projeto no sistema de estimativas Xproject
						para ter acesso <a href="http://pomodorando.com/xproject/view/inicio/?invite='.$conviteProjeto->getKeyInvite().'">clique</a>';

			$libMail->setBodyMail($message);

			$libMail->execute();
		}
		
		public function sendMailConviteProjetoUsuarioExistente(Request $request)
		{
			$controllerConviteProjeto = new ControllerConviteProjeto();
			$controllerUsuario = new ControllerUsuario();
			try 
			{
				$conviteProjeto = $controllerConviteProjeto->getAction()->getConviteByIdProjetoAndIdUsuario($request);
				$usuario = $controllerUsuario->getAction()->getUsuarioById($request);
				
				$libMail = new LibMail();
				$libMail->IsHTML();
				$libMail->debugSmtpOff();
				$libMail->sendTo($usuario->getEmail());
				$libMail->setAltInform('Plataforma de Estimativas Xproject');
				$libMail->setSubject('Convite');
				
				$message = 'Voc� foi convidade a participar de um projeto no sistema de estimativas de projetos Xproject
								para ter acesso <a href="http://pomodorando.com/xproject/view/inicio/index.php?option='.$conviteProjeto->getKeyInvite().'">clique</a>';
					
				$libMail->setBodyMail($message);
				
				if(!$libMail->execute())
					throw new NotMailException('Falha ao notificar usu&aacute;rio');
				
				
			}catch (NoResultException $error){
				throw new NotMailException('Falha nos dados do usu&aacute;rio');
			}
			
			
			 
			if($conviteProjeto && $usuario)
			{
				
			}
		}
}