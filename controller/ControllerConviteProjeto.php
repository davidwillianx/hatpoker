<?php
// include_once '../library/Import.php';
Import::action('ActionConviteProjeto');
Import::action('ActionUsuario');
Import::controller('ControllerProjeto');
Import::controller('ControllerUsuarioProjeto');
Import::controller('ControllerMail');
Import::library('Request');

class ControllerConviteProjeto
{

	public function getAction()
	{
		return new ActionConviteProjeto();
	}

	public function listConvitesProjetoAtivoByIdUsuarioJson(Request $request)
	{
		$convites = $this->getAction()->getAllConviteAtivoByIdUsuario($request);
		if($convites)
		{
			$response = '';
			
			foreach ($convites as $convite)
			{
				$request->set('id', $convite['idProjeto']);
				$controllerProjeto = new ControllerProjeto();
				$projeto = $controllerProjeto->getAction()->getProjeto($request);
					
				if($projeto)
					$response .= '<div class="convite" id="'.$convite['idProjeto'].'">Voce foi convidado a participar do projeto <strong>'.$projeto['descricao'].'</strong><br><br>	
							<a href="#"  id="yes" class="bt bt-blue"> Aceitar</a> <a href="#" id="no" class="bt bt-blue"> Recusar</a></div>';
			}
			
			echo json_encode(array('invite'=>1,'response'=>$response));
			
		}else echo json_encode(array('invite'=>0));
	}
	
	public function ativarConviteProjeto(Request $request)
	{
		$request->set('idUsuario', Session::get('idUsuario'));
		if($this->getAction()->updateConviteAcessoUsuario($request))
			echo 'Participação confirmada';
	}
	
	public function registerConviteProjeto(Request $request)
	{
		if($request->isElement('novoMembro'))
		{
			$validator = new Validator();
			$validator->setElementCondition($request->getKey('email'), 'Nome Usuario', 'required;');
			$validator->setElementCondition($request->getKey('nome'), 'Nome Usuario', 'required;');

			if($validator->isValid())
				if($conviteProjeto = $this->getAction()->persistConviteProjeto($request))
					echo 'Cadastro realizado com sucesso';
			else  $validator->showErros();
		}
	}
	
	public function registerConviteUsuarioExistente(Request $request)
	{
		$successMessage = 'Opera&ccedil;&atilde;o realizada com sucesso';

		try {
			
			$this->getAction()->setConviteProjetoUsuario($request);
			
			$controllerMail = new ControllerMail();
			$controllerMail->sendMailConviteProjetoUsuarioExistente($request);
			Json::send(array('success'=>$successMessage));
			
		}catch (NoPersistException $error){
			Json::send(array('error'=>$error->getMessage()));
		}catch (NotFoundException $error){
			Json::send(array('error'=>$error->getMessage()));
		}
		catch (NotMailException $error){
			Json::send(array('error'=>$error->getMessage()));
		}
	}
	
	



}