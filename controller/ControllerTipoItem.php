<?php

// include_once '../library/Import.php';
Import::library('Request');
Import::action('ActionTipoItem');

class ControllerTipoItem 
{
	public function getAction()
	{
		return new ActionTipoItem();
	}
	
	public function buildOption()
	{
		$tiposItem = $this->getAction()->getAll();
		
		$select = '<select name="categoria" id="idTipoItem">';
		$select .= ' <option value="0">Selecione</option>';		
		foreach ($tiposItem as $tipoItem)
			$select .= '<option value="'.$tipoItem->getId().'" >'.$tipoItem->getDescricao().'</option>';
	
		$select .= ' </select>';
		
		echo $select;
	}
}
?>