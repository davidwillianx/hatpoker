<?php

class Session
{
	
	public static function get($key)
	{
		self::start();
		if(isset($_SESSION[$key]))
			return $_SESSION[$key];
		self::close();
	}

	public static function set($key,$data)
	{
		self::start();
		$_SESSION[$key] = $data;
		self::close();
	}

	public static function isElement($key)
	{
		self::start();
			if(isset($_SESSION[$key]))
				return $_SESSION[$key];
		self::close();

		return false;
	}

	public static function explode()
	{
		var_dump($_SESSION);
	}

	public static function destroy()
	{
		self::start();
		session_destroy();
	}

	private static function start()
	{
		if(!isset($_SESSION))
			session_start();
	}

	private static function close()
	{
		session_write_close();
	}
}