<?php

interface Imail
{
	public function sendTo($address);
	public function setSubject($subject);
	public function setAltInform($alt);
	public function setBodyMail($address);
	public function execute();
	
	
}