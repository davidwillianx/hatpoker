<?php

class Json
{
	private static function encode(array $data)
	{
		return json_encode($data);
	}
	
	public static function decode($data)
	{
		json_decode($data);
	}
	
	public static function send(array $data)
	{
		echo self::encode($data);
	}
}
