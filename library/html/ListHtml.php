<?php

class ListHtml
{ 
	public static function openList($class=null,$id=null)
	{
		
		echo '<ul class="'.$class.'" id="'.$id.'">';
	}

	public static function closeList()
	{
		echo '</ul>';
	}

	public static function openLi($class=null,$id=null)
	{
		echo '<li class="'.$class.'" id="'.$id.'">';
	}

	public static function closeLi()
	{
		echo '</li>';
	}

	public static function liContent($content,$class=null,$id=null)
	{
		echo '<li class="'.$class.'" id="'.$id.'">'.$content.'</li>';
	}
}
?>
 