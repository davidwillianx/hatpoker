 <?php 
 include_once '../library/Import.php';
 Import::library('Navigation');
 Import::library('Session');

 class Security
 {
 	public static function access()
 	{
 		if(!Session::isElement('idUsuario'))
 		{
 			session_destroy();
 			Navigation::sendTo('acesso.php');
 		}
 	}
 }

 ?>