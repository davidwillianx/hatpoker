<?php

class BuildSqlQuery
{
	private $objectReference;

	public function BuildSqlQuery($objectInstantiate)
	{
		$this->objectReference = $objectInstantiate;
	}

	public function insertQuery()
	{
		$reflectionClass = new reflectionClass($this->objectReference);
		$properties = $reflectionClass->getProperties(ReflectionProperty::IS_PRIVATE);
					
		$stringSave = 'INSERT INTO '.lcfirst($reflectionClass->getName()).'(';


		foreach ($properties as $property) {
			if($property->getName() != 'id')	
				$stringSave .= ' `'.$property->getName().'`,';
		}				
				
		$stringSave = substr($stringSave,0,-1);
		$stringSave .= ' ) VALUES ( ';
				

		foreach ($properties as $property) {
			if($property->getName() != 'id')
				$stringSave .= '? ,';
		}

		$stringSave = substr($stringSave, 0, -1).');';
	
		return $stringSave;
	}

	public function selectQuery()
	{
		$reflectionClass = new reflectionClass($this->objectReference);
		$properties = $reflectionClass->getProperties(ReflectionProperty::IS_PRIVATE);

		$stringSave = 'SELECT ';

		foreach ($properties as $property) {
			$stringSave .= ' '.$property->getName().',';
		}		

		$stringSave = substr($stringSave,0,-1);
		$stringSave .= ' FROM '.lcfirst($reflectionClass->getName()).';';

		return $stringSave;
	}

	public function selectQueryWithWhere($where)
	{
		$stringSave = $this->selectQuery();
		$stringSave = substr($stringSave, 0, -1);
		$stringSave .= ' WHERE '.$where.' ;';

		return $stringSave;
	}

	public function selectCountWithWhere($where)
	{
		$stringSave = 'SELECT count(*)';
		$reflectionClass = new reflectionClass($this->objectReference);
		$stringSave .= ' FROM '.lcfirst($reflectionClass->getName()).';';

		return $stringSave;
	}
}

?>