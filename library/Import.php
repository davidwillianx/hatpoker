<?php

class Import
{
	private static function getPath()
	{
// 		$filePath = explode("\\",dirname(__FILE__));
// 		$dirPath =  str_replace(array_pop($filePath), '', dirname(__FILE__));
		return $dirPath = dirname(dirname(__FILE__));	
	}

	public function includeFile($filePath)
	{
		include_once self::getPath().'/'.$filePath;
	}
	
	public static function controller($controller)
	{
		$filePath= 'controller/'.$controller.'.php';
		self::includeFile($filePath);
	}
	
	public static function library($library)
	{
		$filePath =  'library/'.$library.'.php';
		self::includeFile($filePath);
	}

	public static function action($action)
	{
		$filePath =  'model/action/'.$action.'.php';
		self::includeFile($filePath);
	}

	public static function bean($bean)
	{
		$filePath = 'model/bean/'.$bean.'.php';
		self::includeFile($filePath);
	}

	public static function dao($dao)
	{
		$filePath = 'model/dao/'.$dao.'.php';
		self::includeFile($filePath);
	}
	
	public static function viewJson($view)
	{
		$filePath = 'view/_pages/json_pages/'.$view.'.php';
		self::includeFile($filePath);
	}
	
	public static function view($view)
	{
		$filePath = 'view/_pages/'.$view.'.php';
		self::includeFile($filePath);
	}
	
	public static function config()
	{
		$filePath = 'config/Configuration.php';
		self::includeFile($filePath);
	}
}