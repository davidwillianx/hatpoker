$(document).ready(function(){
	saveItem();
});


function saveItem(){
	
//	var isValidated  = isValidated();
	
//	alert(isValidated);
	$('#formNovoItem').submit(function(){

		var titulo = $('#titulo').val();
		var descricao = $('#descricao').val();
		var categoria = $('#idTipoItem').val();
		var prioridade = $('#tipoPrioridade').val();
		var idProjeto = getIdProjeto();
		
		
		$.ajax({
			url: "../persistence-item",
			dataType: 'JSON',
			data: {titulo:titulo,descricao:descricao,idTipoItem:categoria,
				prioridade:prioridade,idProjeto:idProjeto},
			type: 'POST',
			async: false,
			success: function(data)
			{
				if(data['persist'] == 1)
				{
					$('#box').html('<img src="">Cadastro realizado com sucesso');
					openModal('#box');
					
					$('.intensProject .jspPane').load('../view/_pages/_itensBacklog.php?idProjeto='+idProjeto,function(response,status,xhr){
						if(status != "error")
						{
							dragAndDrop();
							loadFragment();
						}
					});
				}
				else{
					$('#box').html('Ocorreu alguma falha');
				}
			},
			beforeSend: function ()
			{
				//TODO fazer loading GIF
				$('#box').append('Enviando..');
				
			},
			
			error:function(msg,status,error)
			{
					alert('Error'+status+' >> error '+error);	
			}
		});

		return false;
	});
}

function loadingRegistro()
{
	alert('realizando o procedimento');
}

function loadFragment()
{
	$('#createStory').html('Success');
	loadScriptScroll();
	closeLightBox();
}

function getIdProjeto()
{
	var data = window.location.href;
	var dataReplace = data.split('/');
	return dataReplace.pop();
}