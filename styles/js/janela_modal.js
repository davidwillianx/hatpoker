$(document).ready(function(){

	loadElementsBox();

	$('a[rel=modal]').click(function(e){
		e.preventDefault();
		var id = $(this).attr('href');
		loadPage('../view/_pages/_formNovoItem.php', id,'');
	});

//	$('a[rel=modalProjeto]').click(function(e){
//		e.preventDefault();
//		var id = $(this).attr('href');
//		loadPage('../_page/_formNewProjeto.php', id,'Projeto');
//	});
//	
	$('#close').click(function (e) {  
		e.preventDefault();  
		closeLightBox();
	}); 
	
	$('#mask').click(function(){
		closeLightBox();
	});
	
});

function loadPage(uri,id,option)
{
	$('#box').load(uri,option,function(response, status, xhr){
		if(status != 'error')
		{
			setDataBox();
			openModal(id);
			
			if(option === 'Item')
				saveItem();
			if(option === 'Projeto')
				persistProjeto();
		}
	});
}

function openModal(element){

	buildMask();

	var heightElement = $(window).height()/2-$(element).height()/2;
	var widthElement = $(window).width()/2-$(element).width()/2;

	$(element).css({'top':heightElement,'left':widthElement}); 
	$(element).fadeIn(500);
}

function closeLightBox()
{
	$('#mask').fadeOut(450);
	$('.window').hide();
	$('#box').fadeOut(400);
}

function builBox()
{
	$('body').append('<div class="box " id="box"></div>');
}

function buildMask()
{
	var heightWindow = $(document).height();
	var widthWindow = $(window).width();

	$('#mask').css({'width':widthWindow,'height':heightWindow});
	$('#mask').fadeIn(400);
	$('#mask').fadeTo("slow",0.5); 
}

function loadElementsBox()
{
	$('body').append('<div id="mask"></div>');
	$('body').append('<div class="box box-shadow" id="box"></div>');
}