$(document).ready(function()
{
	chat.start();
});

var chat = {

	data: {
		lastID: 0,
		noActivity: 0
	},

	start: function()
	{
		/*(function getChats(){}());
		*/
		var working = false;

		$('#submitForm').submit(function(e)
		{
			e.preventDefault();

			var inputMessage = $('#inputMessage'); 
			var msgText = inputMessage.val();
						
			if(msgText == 0 ) 
				return false;
			//if(working) 
			//	return false;

			//working = true;
			

			var tempId = 't'+Math.round(Math.random()*1000000);

			dataMessage = {
				id : tempId,
				msg: msgText
				//some data to add
			}

			//chat.addLine($.extend({},dataMessage));

			$.dataPost('sendMessage',$(this).serialize(),function(respose)
				{
					working = false;
					dataMessage['id'] = respose.insertID;
					chat.addLine($.extend({},dataMessage));
					inputMessage.val('null');
					
				});

			console.log(working);
		});

		(function getUsersTimeStack(){
			chat.getUsers(getUsersTimeStack);
		})();

		(function getMessageTimeStack(){
			chat.getMessage(getMessageTimeStack);
		})();
	},

	getUsers: function(callback)
	{
		$.dataGet('getUsers',function(dataResponse){
			var users = [];

			for (var i = 0; i < dataResponse.users.length; i++) {
				users.push(chat.render('user',dataResponse.users[i]));
			};
		
			if(users.length < 1)
			   console.log('Nao tem ninguem logado');


			$('#chatUsers').html(users.join(' '));
		
			setTimeout(callback,5000);
		});
	},

	getMessage: function(callback){

		$.dataGet('getMessage',{lastID: chat.data.lastID},
		function(response){
		
			for (var i = 0; i < response.chats.length; i++) {
				chat.addLine(response.chats[i]);
			};

			if(response.chats.length)
			{
				chat.data.lastID = response.chats[i - 1].id;
				chat.data.noActivity  = 0 ;
			}else chat.data.noActivity++;

			var nextRequest = 1000;

			if(chat.data.noActivity > 3) nextRequest = 2000;
			if(chat.data.noActivity > 10) nextRequest = 5000;
			if(chat.data.noActivity > 20) nextRequest = 15000;

			setTimeout(callback,nextRequest);
			
		});

	},	

	render: function(option,dataRender)
	{
		var arr = [];
		
		switch(option)
		{
			case 'user':
				arr = ['<div class="user" title="',dataRender.nome,'">',dataRender.nome,'</div>'];
				break;
			case 'line':
                arr = [
                    '<div class="chat chat-',dataRender.idUsuario,' rounded">'+
                    '</span><span class="author">',dataRender.author,
                    ':</span><span class="text">',dataRender.text,
                    '</span><span class="time">',dataRender.time,'</span></div>'];
            break;
		}

		console.log(arr);

		return arr.join('');
	},

	addLine: function(dataMessage)
	{
		var date = new Date();

		if(dataMessage.time)
			date.setUTCHours(dataMessage.time.hours,dataMessage.time.minutes);

		dataMessage.time = (date.getHours() < 10 ? '0' : '' ) + date.getHours()+':'+
                      			(date.getMinutes() < 10 ? '0':'') + date.getMinutes();

     	var newLine = chat.render('line', dataMessage);

     		

     	//Adição temporária
     		$('#boxMessage').append(newLine);

     	//console.log(newLine);
	
        //Append value in box text 
        //reload scrollBar              			
	}

};





$.dataGet = function(action,data,callback)
{
	$.get('../view/_pages/json_pages/_chat?action='+action,data,callback,'json');
}

$.dataPost = function(action,data,callback)
{
	$.post('../view/_pages/json_pages/_chat?action='+action,data,callback,'json');
}

