$(document).ready(function(){
	 $('#slideCycle').cycle({
			fx:     'scrollHorz',
			speed: 	1400,
	    	prev:   '#prev', 
	    	next:   '#next', 
			timeout: 5000
		});
});