$(document).ready(function(){
	$('input:radio[name=invite]').click(function(){
		var option = $(this).val();
		var identify = $(this).attr('id');
	
		var formNovoMembro = '<form action="" method="POST"><div class="form"><label>Nome</label><input type="text" name="nome" size="45"></div><div class="form"><label>Email</label><input type="text" name="email" size="45"></div><div class="form-group"><label>Senha</label><input type="password" name="senha" size="8"><label>Confir.</label><input type="password" name="confirmacao"  size="8"><select name="idPerfil"><option value="0">Selecione</option><option value="1">owner</option><option value="2">team</option></div><input class="bt bt-middle" type="submit" name="registarUsuario" value="Adicionar"></form>';
		var formAddUsuario = '<form action="" method="POST"><div class="form"><label>Email</label><input type="text" name="email" size="45"></div><div class="form"><select name="idPerfil"><option value="0">Selecione</option><option value="1">owner</option><option value="2">team</option></div><input class="bt bt-middle" type="submit" name="adicionarUsuario" value="Adicionar"></form>';
		 
		if(identify === 'novoUsuario' && option === '#new')
				$('.form-components').html(formNovoMembro);
		
		if(identify === 'adicionar' && option === '#add')
				$('.form-components').html(formAddUsuario);
	});
	
	$('a[href=#remove]').click(function(e){
		e.preventDefault();
		var remove =  $(this);
		var href = remove.attr('id');
		var values = href.split('-');
		var id = values[1];
		
		
		if(id)
		{
			$.ajax({
				url: "../view/_pages/json_pages/_desvinculaUsuarioProjeto.php",
				dataType: 'JSON',
				data: {idUsuario:id,idProjeto:getIdProjeto()},
				type: 'POST',
				async: false,
				success: function(data)
				{
					if(data['remove'] == 1)
					{
						alert('Opera��o realizada com sucesso')
						location.reload();
					}
					else{
						alert('falha na opera�ao');
					}
				},
				beforeSend: function ()
				{
					//TODO fazer loading GIF
					$('#box').append('Enviando..');
				},
				
				error:function(msg)
				{
						alert('error ');
						
				}
			});
		}
	});
});

function getIdProjeto()
{
	var data = window.location.href;
	var dataReplace = data.split('/');
	return dataReplace.pop();
}
