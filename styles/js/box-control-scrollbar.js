
$(document).ready(function(){
	$('.intensProject').jScrollPane({
	horizontalGutter:5,
	verticalGutter:5,
	'showArrows': false
	});
	
	$('.sprintItens').jScrollPane({
		horizontalGutter:5,
		verticalGutter:5,
		'showArrows': false
		});
	
	$('.jspDrag').hide();
	$('.jspScrollable').mouseenter(function(){
		$('.jspDrag').stop(true, true).fadeIn('slow');
	});
	$('.jspScrollable').mouseleave(function(){
		$('.jspDrag').stop(true, true).fadeOut('slow');
	});

});
	