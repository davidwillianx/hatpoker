$(document).ready(function(){
	dragAndDrop();
});


 function dragAndDrop()
 {
	 dragItem();
	 dropItem();
 }
function dropItem()
{
	$('.intensProject .element').draggable({
		revert: 'invalid',
		helper: 'clone',
		appendTo: 'body',
	});
}

function dragItem()
{
	$('.sprint #itensRun').droppable({

		drop: function (event,ui)
		{
			var idItem  = $(ui.draggable).attr('id');
			$('.sprint #itensRun .jspPane').append(ui.draggable);
			loadScriptScroll();
			$.ajax({
				url: "../view/_pages/json_pages/_atulizacaoStatusItem.php",
				dataType: 'json',
				type: 'POST',
				data: {id:idItem},
				success: function (data)
				{
					if(data['success']){
						alert('Movimentação realizada com sucesso!');
					}
					else 
						alert('nao deu certo');
				},
				beforeSend: function()
				{
					//TODO  Loading <callback>
				}
			});
		}
	});
}