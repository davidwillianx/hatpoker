
$(document).ready(function(){
	setDataBox();
});

function setDataBox()
{
	$( "#dataInicio" ).datepicker({ dateFormat: "yy-mm-dd" });
	$( "#dataFim" ).datepicker({ dateFormat: "yy-mm-dd" });
}

function loadScriptScroll()
{
	$.getScript('../styles/js/external/jquery.jscrollpane.mim.js');
	$.getScript('../styles/js/external/jquery.mousewheel.js');
	$.getScript('../styles/js/box-control-scrollbar.js');
}

function loadScriptProjeto()
{
	$.getScript('../styles/js/projectManager.js');
}

function getIdProjeto()
{
	var data = window.location.href;
	var dataReplace = data.split('/');
	return dataReplace.pop();
}


